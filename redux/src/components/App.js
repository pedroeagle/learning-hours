import './App.css';
import { Route, Routes } from "react-router-dom";
import Header from "./common/Header";
import PageNotFound from "./PageNotFound";
import CoursesPage from "./courses/CoursesPage";
import HomePage from "./home/HomePage";
import AboutPage from "./about/AboutPage";
import ManageCoursePage from './courses/ManageCoursePage';

function App() {
  return (
    <div className="container-fluid">
      <Header />
      <Routes>
        <Route exact path="/" element={<HomePage />} />
        <Route exact path="/about" element={<AboutPage />} />
        <Route exact path="/courses" element={<CoursesPage />} />
        <Route exact path="/course/:slug" element={<ManageCoursePage />} />
        <Route exact path="/course" element={<ManageCoursePage />} />
        <Route path="*" element={<PageNotFound />} />
      </Routes>
    </div>
  );
}

export default App;
